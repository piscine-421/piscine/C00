/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/09/15 09:11:22 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_print_comb2(void)
{
	char		nbc[5];
	int			index;
	int			nbi;

	index = 0;
	nbi = 0;
	nbc[2] = ' ';
	while (nbi <= 9999)
	{
		if (nbi % 100 > (nbi - nbi % 100) / 100)
		{
			if (index == 1)
			{
				write (1, ", ", 2);
			}
			index = 1;
			nbc[4] = nbi % 10 + 48;
			nbc[3] = ((nbi % 100 - nbc[4] + 48) / 10) + 48;
			nbc[1] = ((nbi - nbi % 100) / 100) % 10 + 48;
			nbc[0] = ((nbi - nbi % 100) / 100 - nbc[1] + 48) / 10 + 48;
			write (1, &nbc, 5);
		}
		nbi += 1;
	}
}

/*int	main(void)
{
	ft_print_comb2();
}*/
