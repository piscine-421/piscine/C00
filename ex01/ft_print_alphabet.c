/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_alphabet.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/09/14 16:50:11 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h> //Include the unistd library to interact with the POSIX API.

void	ft_print_alphabet(void)
//ft_print_alphabet function.
{
	char	alphabet;

	alphabet = 'a';
	while (alphabet <= 'z')
	{
		write(1, &alphabet, 1);
		alphabet += 1;
	}
}
/* Define char array c with entire lowercase alphabet. Use the write function to
 * write data with the following format: fd, an integer with three possible
 * values (0-2), which we set to 1, "standard output", to write the character.
 * Then, buf, which points to the character we're going to write. We specify the
 * same c we defined earlier. Finally, nbytes (26), which is the amount of
 * bytes. We want the entire alphabet and one character = one byte, so the value
 * is 26. */

/* int	main(void)
 * {
 *	ft_print_alphabet();
 * } */
