/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/09/25 13:33:36 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//#include <unistd.h>

void	ft_putnbr(int nb)
{
	char		nbc;
	int			divisor;
	int			start;
	long int	nb2;

	nb2 = nb;
	start = 0;
	divisor = 1000000000;
	if (nb2 < 0)
	{
		write (1, "-", 1);
		nb2 *= -1;
	}
	while (divisor >= 1 && nb != 0)
	{
		nbc = nb2 / divisor % 10 + 48;
		divisor /= 10;
		if (nbc != '0')
			start = 1;
		if (start == 1)
			write (1, &nbc, 1);
	}
	if (nb == 0)
		write (1, "0", 1);
}

/*int	main(void)
{
	ft_putnbr(-2147483648);
	write (1, "\n", 1);
	ft_putnbr(9999);
	write (1, "\n", 1);
	ft_putnbr(0);
	write (1, "\n", 1);
}*/
