/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putchar.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/09/13 14:21:03 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h> //Include the unistd library to interact with the POSIX API.

void	ft_putchar(char c)
//ft_putchar function, contains single character (char) "c".
{
	write(1, &c, 1);
}
/* Use the write function to write data with the following format: fd, an
 * integer with three possible values (0-2), which we set to 1, "standard
 * output", to write the character. Then, buf, which points to the character
 * we're going to write. We specify the same c we defined at the start of the
 * function. Finally, nbytes (1), which is the amount of bytes. We only want a
 * single character and one character = one byte, so the value is 1. */

/* int main(void)
 * {
 *	ft_putchar('Z');
 * } */
