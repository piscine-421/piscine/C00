/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/09/14 13:23:07 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_print_comb(void)
{
	char		numc[3];
	int			numi;
	int			index;

	numi = 0;
	while (numi <= 999)
	{
		numc[2] = numi % 10 + 48;
		numc[1] = numi % 100 / 10 + 48;
		numc[0] = numi / 100 + 48;
		if (numc[1] < numc[2] && numc[0] < numc[2] && numc[0] < numc[1])
		{
			if (index == 1)
			{
				write(1, ", ", 2);
			}
			index = 1;
			write(1, &numc, 3);
		}
		numi += 1;
	}
}

/*int	main(void)
{
	ft_print_comb();
}*/
